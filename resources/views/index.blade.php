<!doctype html>
<html lang="">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    {{--  START SECTION INCLUDE CSS FILES  --}}
    <link href="{{Url('/assets')}}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{Url('/assets')}}/fonts/cairo.css" rel="stylesheet">
    <link href="{{Url('/assets')}}/css/app.css" rel="stylesheet">
    {{--  END SECTION INCLUDE CSS FILES  --}}

    <title> القـرآن الـكريم  </title>
</head>
<br>
<body>

    <div class="container">
        <h1 class="text-center">  القـرآن الـكريم    </h1>

        <br>

        {{--  START READER SECTION --}}
            <div class="form-group">
                <text class="text-sm text-primary">
                    ( يرجي تحديد القاريء قبل إختيار السوره )
                </text>
                <label for="reader"> : إختيار القاريء </label>
                <select class="form-control" id="reader">
                    @foreach($readers['data'] as $reader)
                        <option value="{{ trim($reader['identifier']) }}" {{ ($reader['identifier'] === 'ar.alafasy') ? 'selected' : '' }}>{{ $reader['name'] }}</option>
                    @endforeach
                </select>
            </div>
            {{--  END READER SECTION --}}
        <br>
     
        <hr>

        {{--  AUDIO AREA  --}}
        <audio id="audio" preload="false" tabindex="0" controls=""></audio>
        <ul id="playlist"></ul>
        {{--  AUDIO AREA  --}}

        <div class="row justify-content-start">
            <div class="col-8" id="view"></div>
            <div class="col-4">
                <ul class="list-group">
                    @foreach($data['data'] as $item)
                        <li class="list-group-item" title="عـرض السوره" soura-id="{{ $item['number'] }}"> {{ $item['name'] }} </li>
                    @endforeach
                </ul>
            </div>
        </div>

    </div>

    <script src="{{Url('/assets/js/jquery.js')}}"></script>
    <script src="{{Url('/assets/js/app.js')}}"></script>


    <script !src="">

        // INIT READER VAR VALUE
        let reader = 'ar.alafasy' ;

        /*  IF USER CLICK LI SEND REQUEST TO GET DETAILS  */
        $('ul.list-group li').click(function (e) {
            // e.preventDefault();
            let soura_id = $(this).attr('soura-id') ;
            // CALL SINGLE DETAILS API
            callAjax(soura_id) ;
        });

        /*  THIS METHOD CALL API TO GET DETAILS AND RENDER RESULT TO VIEW  */
        function callAjax(soura_id) {
            let details = "" ;
            $.ajax({
                url     : "{{ route('getSouraContent') }}",
                data    : {
                  "soura_id" : soura_id,
                  "reader"   : reader
                },
                success : function (data) {

                    if (! data.status === true) {
                        alert(data.message) ;
                        return ;
                    }

                    details    = data.data;

                    let _text  = '' ;
                    let _audio = '';

                    for (let i = 0, ii = details.data.numberOfAyahs; i < ii ; i++) {
                        
                        _text += '<div class="col-4 pull-right" >' + details.data.ayahs[i].text + '</div>';
                        _text += '<div class="col-4 pull-left"><audio controls><source src=' + details.data.ayahs[i].audio + '></audio></div>';

                        _text  += '<br><br>';

                        // SET AUDIO PLAYLIST URLS 

                        _audio  += '<li><a href="'+ details.data.ayahs[i].audio +'"></a></li>'; 

                    }

                        $('div#view').html(_text);
                        $('ul#playlist').html(_audio);


                    // FIRE AUDIO PLAYLIST 
                    init();
                },
                beforeSend: function() {
                    // TODO :: SET SOMETHING LIKE LOADER OR LOADING , PLEASE WAIT AND SO ON ^_^
                },
            });
            return details;
        }

        /*  READER SECTION */
        $("select"). change(function(){
            reader = $(this). children("option:selected").val();
        });


        /*  ADUIO AREA */

        function init() {

            var current  = 0;
            var audio    = $('#audio');
            var playlist = $('#playlist');
            var tracks   = playlist.find('li a');

            var len      = tracks.length;

            audio[0].volume = 1;
            audio[0].play();
            

            link    = playlist.find('li a').first();
            current = link.parent().index();
            
            run(link, audio[0]);

            audio[0].addEventListener('ended',function(e) {
                current++;
                if(current == len){
                    current = 0;
                   link = playlist.find('li a').first();
                   //audio[0].stop();
                } else {
                    link = playlist.find('li a')[current];    
                }

                run($(link),audio[0]);
            });
        }

        function run(link, player) {
            player.src = link.attr('href');
            player.load();
            player.play();
        }

        /*  ADUIO AREA */

    </script>

</body>
</html>
