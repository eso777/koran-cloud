<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class TheKoranController extends Controller
{
    /**
     * @uses Calling API To get The Koran surahs list IF NOT EXISTS IN CACHE
     * @use  CACHE_DRIVER = file Write now May be update it to Redis
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        // CHECK CACHE IF HAS DATA BEFORE OF CALL API AND SET IT IN THE CACHE
        $data = Cache::rememberForever('theKoran', function () {
            return  json_decode(file_get_contents('http://api.alquran.cloud/v1/surah'),true) ;
        });

        $readers = Cache::rememberForever('readers', function () {
            return  json_decode(file_get_contents('http://api.alquran.cloud/edition/format/audio'),true) ;
        });

        // RETURN TO VIEW WITH ALL DATA
        return view('index',compact('data', 'readers')) ;
    }

    public function getSouraContent(Request $request)
    {
        $soura_id   = $request->soura_id ;
        $readerName = $request->reader ;

        // VALIDATION SECTION
        if (! in_array($soura_id, range(1,114)) ) {
            return ['status'  => false,'message' => 'عفوً .. يجب أن يكون رقم الصوره بين 1 : 114 ', 'data' => []] ;
        }

        // CHECK IF CONTENT FOR CURRENT TARGET CALLED BEFORE AND EXISTS IN THE CACHE OR CALL IT
        $cacheKey = 'soura_'. $soura_id . '_' . 'reader_' . $readerName ;
        $content  = Cache::rememberForever($cacheKey, function () use ($soura_id, $readerName) {
            // http://api.alquran.cloud/v1/surah/114/ar.alafasy
            return  json_decode(file_get_contents('http://api.alquran.cloud/v1/surah/'. $soura_id . '/'. $readerName),true) ;
        });

        // RETURN WITH DATA
        return ['status'  => true, 'message' => 'Done', 'data' => $content] ;
    }

}
